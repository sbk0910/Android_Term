package com.example.sbk.nsd;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

// loading activity

public class LoadingActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        TextView loading = (TextView)findViewById(R.id.loading);        // 로딩 글자
        loading.setTypeface(Typeface.createFromAsset(getAssets(),"kyoungpil.ttf")); // 글씨체 바꾸기

        //로딩화면 지연시간을 위한 handler
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {    // 1.5초후에 run()함수를 실행-> main activity로 넘어간다.
            @Override
            public void run() {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                finish();

            }
        }, 1500);
    }
}
