package com.example.sbk.nsd;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by sbk on 2017-05-07.
 */

public class DBAdapter extends CursorAdapter{
    private Context mContext = null;
    public DBAdapter(Context context, Cursor c){
        super(context, c);
        mContext = context;
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.list_item, parent, false);

        return v;
    }

    public void bindView(View view, Context contet, Cursor cursor){
        final TextView location = (TextView)view.findViewById(R.id.list_text);
        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "kyoungpil.ttf");
        location.setTypeface(tf);

        location.setText(cursor.getString(cursor.getColumnIndex("location")));
    }
}
