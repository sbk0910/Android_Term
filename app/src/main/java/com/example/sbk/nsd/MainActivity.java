package com.example.sbk.nsd;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public Button search_btn;       // 길찾기 버튼
    public TextView dest_search;
    public TextView sour_search;

    final static int Main_Search = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sour_search = (TextView) findViewById(R.id.sour_search);    // 출발지 글자
        dest_search = (TextView) findViewById(R.id.dest_search);    // 목적지 글자
        search_btn = (Button) findViewById(R.id.search_btn);    // 길찾기 버튼

        // 글씨체 바꾸기
        Typeface tf = Typeface.createFromAsset(getAssets(), "kyoungpil.ttf");
        sour_search.setTypeface(tf);
        dest_search.setTypeface(tf);
        search_btn.setTypeface(tf);

        // button onclick listener
        dest_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   // 목적지 text 클릭 시 search1 activity로 이동
                Intent intent = new Intent(getApplicationContext(), Search1Activity.class);
                startActivityForResult(intent, 0);
            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   // 길찾기 버튼 클릭시 출발지와 목적지 text를 경로 선택 activity로 넘겨준다
                Intent intent = new Intent(getApplicationContext(), SelectWay.class);
                intent.putExtra("src", sour_search.getText().toString());
                intent.putExtra("dest", dest_search.getText().toString());
                startActivity(intent);
            }
        });
    }

        protected void onActivityResult(int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if(requestCode==Main_Search && resultCode == RESULT_OK){      // search1 activity에서 받은 값을 목적지 text의 값으로
                dest_search.setText(data.getStringExtra("TextOut"));
            }


    }
}
