package com.example.sbk.nsd;

import android.content.Intent;
import android.graphics.Typeface;
import android.icu.text.DisplayContext;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SelectWay extends AppCompatActivity {
    ViewPager vp;       // 도보와 대중교통을 위한 뷰페이저
    LinearLayout ll;    // 뷰페이저가 들어간 linear layout

    // 경로 선택 activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_way);

        vp = (ViewPager)findViewById(R.id.vp);
        ll = (LinearLayout)findViewById(R.id.ll);

        TextView src = (TextView)findViewById(R.id.src);    // 출발지 글자
        TextView src_text = (TextView)findViewById(R.id.src_text);  // 출발지 값
        TextView dest = (TextView)findViewById(R.id.dest);      // 도착지 글자
        TextView dest_text = (TextView)findViewById(R.id.dest_text);    // 도착지 값
        TextView tab_first = (TextView)findViewById(R.id.tab_first);    // 도보 글자
        TextView tab_second = (TextView)findViewById(R.id.tab_second);  // 대중교통 글자

        // 글씨체 바꾸기
        Typeface tf = Typeface.createFromAsset(getAssets(), "kyoungpil.ttf");
        src.setTypeface(tf);
        src_text.setTypeface(tf);
        dest.setTypeface(tf);
        dest_text.setTypeface(tf);
        tab_first.setTypeface(tf);
        tab_second.setTypeface(tf);

        // main activity에서 넘겨받은 값을 출발지값과 목적지 값으로 설정
        Intent intent = getIntent();
        src_text.setText(intent.getStringExtra("src"));
        dest_text.setText(intent.getStringExtra("dest"));

        // 뷰 페이저 설정을 위한 adapter
        vp.setAdapter(new pagerAdapter(getSupportFragmentManager()));

        // 초기 값을 0으로 설정 (값이 0이면 도보 페이지, 1이면 대중교통 페이지)
        vp.setCurrentItem(0);

        tab_first.setOnClickListener(movePageListener); //첫번째(도보) 탭을 누르면 도보 페이지로 이동
        tab_first.setTag(0);    // 도보탭에 해당하는 값은 0
        tab_second.setOnClickListener(movePageListener);    // 두번째(대중교통) 탭을 누르면 대중교통 페이지로 이동
        tab_second.setTag(1);   // 대중교통 탭에 해당하는 값은 1

        tab_first.setSelected(true);    // 처음에는 첫번째 탭이 선택되어있다

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {   // 페이지 변환 리스너-> 페이지가 바뀌면서 탭도 바뀌도록
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int i = 0;
                while(i < 2){
                    if(position == i){
                        //  페이저의 값과 i가 같으면 해당 탭이 활성화
                        ll.findViewWithTag(i).setSelected(true);
                    } else{
                        ll.findViewWithTag(i).setSelected(false);
                    }
                    i++;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {   // 페이지를 슬라이드? 할 때의 리스너
            int tag = (int)v.getTag();  // tag값은 현재 뷰페이저의 태그값

            int i = 0;
            while(i < 2){
                if(tag == i){
                    // 페이저의 값과 i가 같으면 해당 탭이 활성화
                    ll.findViewWithTag(i).setSelected(true);
                }else{
                    ll.findViewWithTag(i).setSelected(false);
                }
                i++;
            }
            vp.setCurrentItem(tag);
        }
    };

    private class pagerAdapter extends  FragmentStatePagerAdapter{
        public pagerAdapter(android.support.v4.app.FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new SelectFoot();    // 0이면 도보 페이지
                case 1:
                    return new SelectPublic();  // 1이면 대중교통 페이지
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }   // 페이지 개수
    }
}
