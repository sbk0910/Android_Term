package com.example.sbk.nsd;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

// 목적지 클릭 후 activity : 최근검색기록 + 검색 edit

public class Search1Activity extends AppCompatActivity {
    public ImageButton search_dest_btn = null;      // edit 검색 btn
    public ImageButton back_btn = null;              // 뒤로가기 btn
    public ListView list;                             // 최근 검색기록 리스트
    public EditText dest_edit = null;                // 검색 edit

    final static int SEARCH_2 = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search1);

        dest_edit = (EditText)findViewById(R.id.dest_edit);

        TextView dest_text = (TextView)findViewById(R.id.dest_text);    // 목적지 글자
        TextView latest_text = (TextView)findViewById(R.id.latest_text);    // 최근 검색기록 글자


        // 글씨체 바꾸기
        Typeface tf = Typeface.createFromAsset(getAssets(), "kyoungpil.ttf");
        dest_text.setTypeface(tf);
        dest_edit.setTypeface(tf);
        latest_text.setTypeface(tf);



        // array list 설정
        list = (ListView)findViewById(R.id.latest_list);
        // latest_list array에 있는 원소를 리스트의 원소로
        final XmlAdapter Adapter = new XmlAdapter(this, R.layout.list_item, getResources().getTextArray(R.array.latest_list));
        list.setAdapter(Adapter);
        list.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {  // 원소 클릭시 main activity에 원소의 값을 넘겨준다.
                String str = (String) Adapter.getItem(position);
                Intent intent = getIntent();
                intent.putExtra("TextOut", str);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        // button click listener
        search_dest_btn = (ImageButton)findViewById(R.id.search_dest_btn);  // 검색 버튼
        search_dest_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   // 버튼 클릭시 edit에 있는 값을 search2activity에 넘겨준다.
                String str = dest_edit.getText().toString();

                Intent intent = new Intent(getApplicationContext(), Search2Activity.class);
                intent.putExtra("Search", str);
                startActivityForResult(intent, 1);
            }
        });

        back_btn = (ImageButton)findViewById(R.id.back_btn);    // 뒤로가기 버튼
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });






    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==SEARCH_2 && resultCode == RESULT_OK){       // search2 activity에서 넘어온 값을 main activity로 넘겨준다.
            String str = data.getStringExtra("dest");
            Intent intent = getIntent();
            intent.putExtra("TextOut", str);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

}
