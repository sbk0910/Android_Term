package com.example.sbk.nsd;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

// edit 입력 후 activity : 입력된 값에 대한 선택지

public class Search2Activity extends AppCompatActivity {
    public TextView search_enter = null;       // 에디터를 통해 받은 목적지값
    public ListView list = null;                // 검색 리스트

    final static int SEARCH_2 = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search2);

        // search1 activity에서 받은 값을 search_enter text로 설정
        search_enter = (TextView)findViewById(R.id.search_enter);
        Intent intent = getIntent();
        search_enter.setText(intent.getStringExtra("Search"));

        // 글씨체 바꾸기
        Typeface tf = Typeface.createFromAsset(getAssets(),"kyoungpil.ttf");
        search_enter.setTypeface(tf);

        //
        search_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {       // 목적지 text를 누르면 search1 activity로 다시 돌아간다
                Intent before_intent = new Intent(getApplicationContext(), Search1Activity.class);
                finish();
            }
        });


        // array list 설정
        list = (ListView)findViewById(R.id.search_list);
        // latest_list array에 있는 원소를 리스트의 원소로
        final XmlAdapter Adapter = new XmlAdapter(this, R.layout.list_item, getResources().getTextArray(R.array.search_list));
        list.setAdapter(Adapter);
        list.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {    // 리스트 원소 클릭시 search1으로 원소값을 넘겨주고
                String str = (String) Adapter.getItem(position);             // 이것은 search1에서 main acrivity로 넘긴다
                Intent intent = getIntent();
                intent.putExtra("dest", str);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
