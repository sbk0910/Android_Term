package com.example.sbk.nsd;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

// 도보 페이지

public class SelectFoot extends android.support.v4.app.Fragment{
    public SelectFoot(){

        }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // 페이지에 해당하는 레이아웃 설정
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.activity_select_foot, container, false);

        // fool_list array에 있는 원소를 리스트 원소로
        final XmlAdapter Adapter = new XmlAdapter(getActivity().getApplicationContext(), R.layout.list_item, getResources().getTextArray(R.array.foot_list));
        ListView list = (ListView)layout.findViewById(R.id.foot_list);
        list.setAdapter(Adapter);
        return layout;
    }
}
