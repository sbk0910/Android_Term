package com.example.sbk.nsd;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by sbk on 2017-04-21.
 */

//Xml file parsing 하기 위한 arrayadapter

public class XmlAdapter extends ArrayAdapter<CharSequence> {
    private Context  mContext = null;
    private int layoutResourceId;
    private CharSequence data[] = null;
    private LayoutInflater inflater = null;

    public XmlAdapter(Context context, int layoutResourceId, CharSequence[] data){
        // xml parsing한 array list 생성
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext= context;
        this.data = data;
        this.inflater = LayoutInflater.from(mContext);
    }

    // position에 해당하는 원소의 값을 반환
    public CharSequence getItem(int position){
        return super.getItem(position);
    }

    public View getView(int position, View convertview, ViewGroup parent){
        View v = convertview;
        if(v == null){
            v = inflater.inflate(R.layout.list_item, null);
        }

        // 글씨체 바꾸기
        TextView list_text = (TextView)v.findViewById(R.id.list_text);    // array list view 하나의 아이템
        Typeface tp = Typeface.createFromAsset(mContext.getAssets(), "kyoungpil.ttf");

        list_text.setTypeface(tp);
        list_text.setText(getItem(position));
        return v;
    }
}
