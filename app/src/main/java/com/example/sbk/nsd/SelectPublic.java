package com.example.sbk.nsd;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

// 대중교통 페이지

public class SelectPublic extends android.support.v4.app.Fragment{
    public SelectPublic(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedIntaceState){

        // 페이지에 해당하는 레이아웃 설정
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.activity_select_public, container, false);

        TextView pub = (TextView)layout.findViewById(R.id.pub); // 대중교통 없음 글자
        // 글씨체 변경
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "kyoungpil.ttf");
        pub.setTypeface(tf);
        return layout;
    }
}
